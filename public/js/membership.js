
function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var logmem = document.getElementById('logmem');
        
        var membership_email = document.getElementById('membership_email');
        var membership_name = document.getElementById('membership_name');
        var birthdayY = document.getElementById('birthdayY');
        var birthdayM = document.getElementById('birthdayM');
        var birthdayD = document.getElementById('birthdayD');
        var phone = document.getElementById('phone');
        var confirm = document.getElementById('confirm');
        var membership_password = document.getElementById('membership_password');
        
        // Check user login
        if (user) {
            var id = firebase.auth().currentUser;
            var ID = id.uid;
            //navbar顯示帳號
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });

            //上傳大頭貼
            var storageRef = firebase.storage().ref();
            var uploadFileInput = document.getElementById("uploadFileInput");           
            uploadFileInput.addEventListener("change", function(){
                var file = this.files[0];
                var uploadTask = storageRef.child('images/'+ ID).put(file);
                //why doesn't work?
                firebase.storage().ref().child('images/'+ ID).getDownloadURL().then(function(url) {
                    var img = document.getElementById('myimg');
                    img.src = url;
                }).catch(function(error) {
                });
            },false);

            //顯示使用者原始資料
            membership_email.innerHTML = user.email;
            firebase.database().ref('users/' + ID).once('value')
            .then(function (snapshot) {
                membership_name.value = (snapshot.val() && snapshot.val().name) || null;
                membership_password.innerHTML = (snapshot.val() && snapshot.val().password) || null;
                phone.value = (snapshot.val() && snapshot.val().phone) || null;
                birthdayY.value = (snapshot.val() && snapshot.val().birthdayY) || null;
                birthdayM.value = (snapshot.val() && snapshot.val().birthdayM) || null;
                birthdayD.value = (snapshot.val() && snapshot.val().birthdayD) || null;
            })
            .catch(e => console.log(e.message));

            //顯示使用者原始頭貼
            storageRef.child('images/'+ ID).getDownloadURL().then(function(url) {
                var img = document.getElementById('myimg');
                img.src = url;
            }).catch(function(error) {
            });

            //儲存使用者更新後的資料
            confirm.addEventListener('click', function() {
                firebase.database().ref('users/' + ID).set({
                    id : ID,
                    name : membership_name.value,
                    email : user.email,
                    password : membership_password.innerHTML,
                    birthdayY : birthdayY.value,
                    birthdayM : birthdayM.value,
                    birthdayD : birthdayD.value,
                    phone : phone.value,
                }).catch(function(error) {
                    console.log(e.message)
                });
                alert("資料已更新");
            });


        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";
            alert("尚未登入會員");
            window.location.href="signin.html";

        }
    });
}

window.onload = function () {
    init();
};
