function step3(){
    var arr=document.getElementsByName("shipping");
    var select=0;
    for(var i=0;i<arr.length;i++)
    {
        if(arr[i].checked)
        {
            select=1;
            window.location.href = arr[i].value + ".html";
        }
    }
    if(select==0)
        alert("請選擇配送與付款方式");
}

function init() {
    var flag=0;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var cartpage = document.getElementById('cartpage');
        var logmem = document.getElementById('logmem');
        
        // Check user login
        if (user) {
            var id = firebase.auth().currentUser;
            var ID = id.uid;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            cartpage.innerHTML = "<br><h5 class = 'notlogin'>尚未登入會員!</h5><br>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });
        
}
window.onload = function () {
    init();
};
