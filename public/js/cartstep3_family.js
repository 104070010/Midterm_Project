
function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var cartpage = document.getElementById('cartpage');
        var logmem = document.getElementById('logmem');

        var name = document.getElementById('name');
        var phone = document.getElementById('phone');
        var store = document.getElementById('store');
        var others = document.getElementById('others');
        var confirm = document.getElementById('confirm');
        // Check user login
        if (user) {
            var id = firebase.auth().currentUser;
            var ID = id.uid;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });

            firebase.database().ref('orders/' + ID).once('value')
            .then(function (snapshot) {
                name.value = (snapshot.val() && snapshot.val().Name) || null;
                phone.value = (snapshot.val() && snapshot.val().Phone) || null;
                store.value = (snapshot.val() && snapshot.val().Store) || null;
                others.value = (snapshot.val() && snapshot.val().Others) || null;
            })
            .catch(e => console.log(e.message));

            confirm.addEventListener('click', function() {
                firebase.database().ref('orders/' + ID).set({
                    Method : "全家超商取貨付款",
                    Name : name.value,
                    Phone : phone.value,
                    Store : store.value,
                    Others : others.value
                }).catch(function(error) {
                    console.log(e.message)
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            cartpage.innerHTML = "<br><h5 class = 'notlogin'>尚未登入會員!</h5><br>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });
        
}
window.onload = function () {
    init();
};

