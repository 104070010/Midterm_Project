
function initApp() {
    // Login with Email/Password
    var inputName = document.getElementById('inputName');
    var inputEmail = document.getElementById('inputEmail');
    var inputPassword = document.getElementById('inputPassword');
    var inputemail = document.getElementById('inputemail');
    var inputpassword = document.getElementById('inputpassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        var email = inputEmail.value;
        var password = inputPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(user) {
            alert("Login success");
            window.history.go(-1);
            console.log(user);
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            alert(errorMessage);
            console.log(error);
            document.getElementById('inputEmail').value = '';
            document.getElementById('inputPassword').value = '';
        });
    });

    var provider = new firebase.auth.GoogleAuthProvider();
    btnGoogle.addEventListener('click', function () {
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function(user) {
            window.history.go(-1);
            alert("Logout success");
            }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            alert(errorMessage);
            console.log(error); });
    });


    
    btnSignUp.addEventListener('click', function () {  
        var name = inputName.value;
        var email = inputemail.value;
        var password = inputpassword.value;
        var id = " ";
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user) {
            id = firebase.auth().currentUser;
            firebase.database().ref('users/' + id.uid).set({
                id : id.uid,
                name : inputName.value,
                email : inputemail.value,
                password : inputpassword.value
            }).catch(function(error) {
                console.log(e.message)
            });
            window.history.go(-1);
            alert("Login success");
            console.log(user);
            document.getElementById('inputemail').value = '';
            document.getElementById('inputpassword').value = '';
            
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            alert(errorMessage);
            console.log(error);
            document.getElementById('inputemail').value = '';
            document.getElementById('inputpassword').value = '';
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};