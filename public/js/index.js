
function init() {
    //var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var logmem = document.getElementById('logmem');
        // Check user login
        if (user) {
            //user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login / Sign Up</a>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });

    //notification
    Notification.requestPermission().then(function(permission) {
        if(permission === 'granted'){
            console.log('用戶允許通知');
        }else if(permission === 'denied'){
            console.log('用戶拒絕通知');
        }
    });
    
    if(Notification.permission === 'granted'){
        console.log('用戶允許通知');
    }else if(Notification.permission === 'denied'){
        console.log('用戶拒絕通知');
    }else{
        console.log('用戶尚未選擇');
    }
    function addOnBeforeUnload(e) {
        FERD_NavNotice.notification.close();
    }
    if(window.attachEvent){
        window.attachEvent('onbeforeunload', addOnBeforeUnload);
    } else {
        window.addEventListener('beforeunload', addOnBeforeUnload, false);
    }
    
    var n = new Notification('2018 New Arrivals !!',{
        body: '心動不如馬上行動，買到剁手手',
        tag: 'linxin',
        icon: 'img/notification.png',
        requireInteraction: true
    })

}


window.onload = function () {
    init();
};

