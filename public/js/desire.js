function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var cartpage = document.getElementById('cartpage');
        var logmem = document.getElementById('logmem');
        // Check user login
        if (user) {
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });

            var id = firebase.auth().currentUser;
            var ID = id.uid;

            var count = 1;
            firebase.database().ref('likes/' + ID +'/clothes/').once('value').then(function (snapshot) {
                if((snapshot.val() && snapshot.val().clike1)==="1"){
                    document.getElementById('clothes_list').innerHTML+='<tr><th>' + count + "</th><th>氣質削肩高領無袖上衣</th><th>NT$ 490</th><th><a href='1821039-01-01.html' target='1821039-01-01.html'><img src='img/1821039-01-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().clike2)==="1"){
                    document.getElementById('clothes_list').innerHTML+='<tr><th>' + count + "</th><th>CHIAO聯名氣質削肩露背長洋裝</th><th>NT$ 890</th><th><a href='1824121-01-01.html' target='1824121-01-01.html'><img src='img/1824121-01-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().clike3)==="1"){
                    document.getElementById('clothes_list').innerHTML+='<tr><th>' + count + "</th><th>蕾絲緹花透肌傘襬洋裝</th><th>NT$ 590</th><th><a href='1814119-02-01.html' target='1814119-02-01.html'><img src='img/1814119-02-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().clike4)==="1"){
                    document.getElementById('clothes_list').innerHTML+='<tr><th>' + count + "</th><th>綁結交叉無袖短版上衣</th><th>NT$ 490</th><th><a href='1821107-02-01.html' target='1821107-02-01.html'><img src='img/1821107-02-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().clike5)==="1"){
                    document.getElementById('clothes_list').innerHTML+='<tr><th>' + count + "</th><th>CHIAO聯名後綁帶長袖襯衫</th><th>NT$ 590</th><th><a href='1821151-08-01.html' target='1821151-08-01.html'><img src='img/1821151-08-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().clike6)==="1"){
                    document.getElementById('clothes_list').innerHTML+='<tr><th>' + count + "</th><th>抽鬚刷色牛仔口袋襯衫上衣</th><th>NT$ 630</th><th><a href='1811213-04-01.html' target='1811213-04-01.html'><img src='img/1811213-04-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().clike7)==="1"){
                    document.getElementById('clothes_list').innerHTML+='<tr><th>' + count + "</th><th>交叉綁帶條紋襯衫上衣</th><th>NT$ 590</th><th><a href='1811099-04-01.html' target='1811099-04-01.html'><img src='img/1811099-04-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
            })
            .catch(e => console.log(e.message));

            firebase.database().ref('likes/' + ID +'/shoes/').once('value').then(function (snapshot) {
                if((snapshot.val() && snapshot.val().slike1)==="1"){
                    document.getElementById('shoes_list').innerHTML+='<tr><th>' + count + "</th><th>花猴聯名滾邊絨布尖頭粗跟短靴</th><th>NT$ 1222</th><th><a href='1740027-05-01.html' target='1740027-05-01.html'><img src='img/1740027-05-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().slike2)==="1"){
                    document.getElementById('shoes_list').innerHTML+='<tr><th>' + count + "</th><th>CHIAO聯名亮面細條繞踝高跟鞋</th><th>NT$ 1580</th><th><a href='1820015-23-06b.html' target='1820015-23-06b.html'><img src='img/1820015-23-06b.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
            })
            .catch(e => console.log(e.message));

            firebase.database().ref('likes/' + ID +'/neckless/').once('value').then(function (snapshot) {
                if((snapshot.val() && snapshot.val().nlike1)==="1"){
                    document.getElementById('neckless_list').innerHTML+='<tr><th>' + count + "</th><th>韓國扭轉造型金屬頸鍊</th><th>NT$ 290</th><th><a href='17062045-22-04b.html' target='17062045-22-04b.html'><img src='img/17062045-22-04b.jpg' style='width:5%'></a></th></tr>";
                    count+=1;
                }
                if((snapshot.val() && snapshot.val().nlike2)==="1"){
                    document.getElementById('neckless_list').innerHTML+='<tr><th>' + count + "</th><th>韓國毛線編織十字鑽飾頸鍊</th><th>NT$ 249</th><th><a href='17061947-67-04b.html' target='17061947-67-04b.html'><img src='img/17061947-67-04b.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
            })
            .catch(e => console.log(e.message));

            firebase.database().ref('likes/' + ID +'/pants/').once('value').then(function (snapshot) {
                if((snapshot.val() && snapshot.val().plike1)==="1"){
                    document.getElementById('pants_list').innerHTML+='<tr><th>' + count + "</th><th>顯瘦破壞A字牛仔短褲</th><th>NT$ 590</th><th><a href='1813049-01-01.html' target='1813049-01-01.html'><img src='img/1813049-01-01.jpg' style='width:10%'></a></th></tr>";
                    count+=1;
                }
            })
            .catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            cartpage.innerHTML = "<br><h5 class = 'notlogin'>尚未登入會員!</h5><br>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });
}

window.onload = function () {
    init();
};

