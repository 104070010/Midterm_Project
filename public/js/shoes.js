
function like1(){
    var slike1 = document.getElementById('slike1');
    var slike2 = document.getElementById('slike2');
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var id = firebase.auth().currentUser;
            var ID = id.uid;
            if(slike1.alt==="0"){
                slike1.src = "img/liked.png";
                firebase.database().ref('likes/' + ID  + '/shoes/').set({
                    slike1 : "1",
                    slike2 : slike2.alt,
                }).catch(function(error) {
                    console.log(e.message)
                });
                slike1.alt="1";
            } 
            else{
                slike1.src = "img/like.png";
                firebase.database().ref('likes/' + ID  + '/shoes/' + '/slike1/').remove().then(function(){
                    console.log("成功刪除")
                });
                slike1.alt="0";
            }
        } else {
            alert("尚未登入會員");
        }
    });
}
function like2(){
    var slike1 = document.getElementById('slike1');
    var slike2 = document.getElementById('slike2');
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var id = firebase.auth().currentUser;
            var ID = id.uid;
            if(slike2.alt==="0"){
                slike2.src = "img/liked.png";
                firebase.database().ref('likes/' + ID  + '/shoes/').set({
                    slike1 : slike1.alt,
                    slike2 : "1",
                }).catch(function(error) {
                    console.log(e.message)
                });
                slike2.alt="1";
            } 
            else{
                slike2.src = "img/like.png";
                firebase.database().ref('likes/' + ID + '/shoes/' + '/slike2/').remove().then(function(){
                    console.log("成功刪除")
                });
                slike2.alt="0";
            }
        } else {
            alert("尚未登入會員");
        }
    });
}
function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var logmem = document.getElementById('logmem');
        // Check user login
        if (user) {
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });

            var id = firebase.auth().currentUser;
            var ID = id.uid;

            firebase.database().ref('likes/' + ID + '/shoes/').once('value').then(function (snapshot) {
                if((snapshot.val() && snapshot.val().slike1)==="1"){
                    slike1.src = "img/liked.png";
                    slike1.alt = "1";
                }
                if((snapshot.val() && snapshot.val().slike2)==="1"){
                    slike2.src = "img/liked.png";
                    slike2.alt = "1";
                } 
            })
            .catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });
}

window.onload = function () {
    init();
};


   
