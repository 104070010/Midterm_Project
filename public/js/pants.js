
function like1(){
    var plike1 = document.getElementById('plike1');
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var id = firebase.auth().currentUser;
            var ID = id.uid;
            if(plike1.alt==="0"){
                plike1.src = "img/liked.png";
                firebase.database().ref('likes/' + ID  + '/pants/').set({
                    plike1 : "1",
                }).catch(function(error) {
                    console.log(e.message)
                });
                plike1.alt="1";
            } 
            else{
                plike1.src = "img/like.png";
                firebase.database().ref('likes/' + ID  + '/pants/' + '/plike1/').remove().then(function(){
                    console.log("成功刪除")
                });
                plike1.alt="0";
            }
        } else {
            alert("尚未登入會員");
        }
    });
}
function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var logmem = document.getElementById('logmem');
        // Check user login
        if (user) {
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });

            var id = firebase.auth().currentUser;
            var ID = id.uid;


            firebase.database().ref('likes/' + ID + '/pants/').once('value').then(function (snapshot) {
                if((snapshot.val() && snapshot.val().plike1)==="1"){
                    plike1.src = "img/liked.png";
                    plike1.alt = "1";
                }
                    
            })
            .catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });
}

window.onload = function () {
    init();
};


