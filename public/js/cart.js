function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var cartpage = document.getElementById('cartpage');
        var logmem = document.getElementById('logmem');
        // Check user login
        if (user) {
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });

            var id = firebase.auth().currentUser;
            var ID = id.uid;

            var str1 = "</th><th>";
            var str2 = "</th><th><span class='glyphicon glyphicon-trash'></span></th></tr>";
            var postsRef = firebase.database().ref('items/' + ID);
            var total_post = [];
            var count = 1;
            var total = 0;

            postsRef.once('value').then(function (snapshot) {
                snapshot.forEach(function(childSnapshot){
                    var childData = childSnapshot.val();
                    total_post[total_post.length] = "<tr><th>" + count + str1 + childData.item + str1 + childData.color
                                                    + str1 + childData.size + str1 + childData.quantity + str1 + '$' + 
                                                    childData.dollareach + str1 + '$' + childData.dollar + str2;
                    count += 1;
                    total = total + childData.dollar;
                });
                document.getElementById('post_list').innerHTML = total_post.join('');
                document.getElementById('total').innerHTML = "總額 : NT$  " + total;
            })
            .catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            cartpage.innerHTML = "<br><h5 class = 'notlogin'>尚未登入會員!</h5><br>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });
}

window.onload = function () {
    init();
};

