function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var cartpage = document.getElementById('cartpage');
        var logmem = document.getElementById('logmem');

        var name = document.getElementById('name');
        var method = document.getElementById('method');
        var phone = document.getElementById('phone');
        var others = document.getElementById('others');
        var content = document.getElementById('content');

        // Check user login
        if (user) {
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span>";
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function() {
                firebase.auth().signOut().then(function(user) {
                    alert("Logout success");
                    console.log(user);
                    }).catch(function(error) {
                    // Handle Errors here.
                    alert("Logout fail");
                      console.log(error);
                    
                  });
            });
            var id = firebase.auth().currentUser;
            var ID = id.uid;

            var str1 = "</th><th>";
            var str2 = "</th><th><span class='glyphicon glyphicon-trash'></span></th></tr>";
            var postsIte = firebase.database().ref('items/' + ID);
            var total_post = [];
            var count = 1;
            var total = 0;

            postsIte.once('value').then(function (snapshot) {
                snapshot.forEach(function(childSnapshot){
                    var childData = childSnapshot.val();
                    total_post[total_post.length] = "<tr><th>" + count + str1 + childData.item + str1 + childData.color
                                                    + str1 + childData.size + str1 + childData.quantity + str1 + '$' + 
                                                    childData.dollareach + str1 + '$' + childData.dollar + str2;
                    count += 1;
                    total = total + childData.dollar;
                });
                document.getElementById('post_list').innerHTML = total_post.join('');
                document.getElementById('total').innerHTML = "總額 : NT$  " + total;
            })
            .catch(e => console.log(e.message));

            var str3=", ";
            firebase.database().ref('orders/' + ID).once('value')
            .then(function (snapshot) {
                method.innerHTML = (snapshot.val() && snapshot.val().Method) || null;
                name.innerHTML = (snapshot.val() && snapshot.val().Name) || null;
                phone.innerHTML = (snapshot.val() && snapshot.val().Phone) || null;
                others.innerHTML = (snapshot.val() && snapshot.val().Others) || null;
                if(method.innerHTML=="7-11超商取貨付款"){
                    content.innerHTML = (snapshot.val() && snapshot.val().Store) || null;
                }else if(method.innerHTML=="全家超商取貨付款"){
                    content.innerHTML = (snapshot.val() && snapshot.val().Store) || null;
                }else if(method.innerHTML=="信用卡線上刷卡"){
                    content.innerHTML = (snapshot.val() && snapshot.val().Address) || null;
                }else {
                    content.innerHTML = (snapshot.val() && snapshot.val().Address) || null;
                }
            })
            .catch(e => console.log(e.message));

            var complete = document.getElementById("complete");
            complete.addEventListener("click", function(){
                firebase.database().ref('/items/' + ID).remove().then(function(){
                console.log("成功刪除")
              });
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            cartpage.innerHTML = "<br><h5 class = 'notlogin'>尚未登入會員!</h5><br>";
            logmem.innerHTML = "<li class='nav-item dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><span class='glyphicon glyphicon-user'></span> Your Account<span class='caret'></span></a><ul class='dropdown-menu'><li id='dynamic-menu'><a class='dropdown-item' href='signin.html'>Login / Sign Up</a></li> </ul></li><li><a href='cart.html'><span class='glyphicon glyphicon-shopping-cart'></span> Cart</a></li>";

        }
    });
}

window.onload = function () {
    init();
};

