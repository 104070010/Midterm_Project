# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Bonny Online-store]
* Key functions (add/delete)
    1. Product page
    2. shopping pipeline
    3. user dashboard
* Other functions (add/delete)
    1. 可上傳照片(頭貼)並顯示於會員資料中
    2. 可更改會員資料
    3. 喜歡的商品可以按愛心讓他變成紅色，並且在下次登入時依然保持紅色
    4. 可將喜歡的商品加入慾望清單中，並顯示出來
    5. 訂單完成前，可確認訂單的詳細資料
    6. 在任何頁面Login後，都可以直接回到原頁面
    7. 點選商品會開啟新分頁




## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
Bonny Online-store 是一個以"版面簡潔俐落"、"使用者操作方便"為訴求的網頁，販賣衣服、鞋子、褲子、項鍊四大類商品。
在每個頁面中都可以看到Bonny比著讚旋轉，很可愛，而以黑白灰及大理石為主的配色也可以避免使用者逛太久時覺得眼花撩亂。

1. 基本架構：每一頁都會有header和navbar，且滑動時，navbar會停留在網頁的最上方，讓使用者可以更方便的跳轉。
每一頁最上方的header放的是Bonny Online-store的LOGO，我將它設計成一隻比著讚的、快樂旋轉著的兔子~
接下來是一條navbar，包含home、四大類商品、購物車以及會員登入(慾望清單、會員資料、登出則是在登入狀態時才會顯現在navbar中)。

2. "Home" : body的上半部分則放了可手動也可自動切換的商品圖片Carousel，下半部分放置其他商品圖片及折扣提醒圖。

3. "Your Account" : 在會員登入頁中，包含兩個部分，一個是sign in，一個是register。
左半邊的Sign in又分為用之前註冊的帳號sign in以及用google帳號sign in ; 右半邊則是註冊新會員。
登入後，會直接跳轉到首頁，並且在navbar中"Your Account"的dropdown menu改為顯示使用者的帳號。

4. "Membership" : Sign in後就可以到這裡查看會員資料，也可以新增或更改電話、姓名、生日等資料。
另外我利用firebase的storage，讓使用者也可以上傳或更改自己的大頭貼。

5. "Clothes"、"Shoes"、"Neckless"、"Pants" : 四大類商品的頁面設計很像，這裡以clothes來做說明。
在body的上半部，會先放上一張以clothes為主題的橫幅，接下來才是商品細項，一個row放四個商品，每個商品包含圖片、名稱及價格。
點任一商品進入商品頁面時，我是用開啟分頁的方式來進入，這樣就可以避免一直按上一頁或不停地複製連結，讓使用者可以更方便的開啟多個網頁來比較商品。
在商品頁面中，除了商品圖片、名稱、價格外，使用者還可以選擇顏色、尺寸、數量，並加入購物車。
此外也有包含商品的簡介及更多參考圖片在下方，也可以點擊size guide，就會跳出modal-dialog來觀看參考尺寸表。

6. "Cart" : 購物車分為四個步驟，每一個步驟使用者都可以選擇"繼續購物"、"上一步"、"下一步"，方便使用者填錯時做更改。
    (1) "確認購買明細" : 一進入購物車就是在第一個步驟，使用者可以看見自己選購的所有商品的編號、名稱、顏色、尺寸、數量、單價、小計等詳細資料，
    此外也會幫使用者算出目前的商品總額。
    (2) "配送與付款方式" : 這個步驟有四個選項，沒有選取的話就不能到下一個步驟。
    四個選項包含7-11、全家超商取貨付款、信用卡線上刷卡，和宅配貨到付款。
    (3) "填寫收件資料" : 四種付款方式都須填寫姓名、手機、備註欄。
    超商取貨付款需額外填寫取貨門市，我有附上門市查詢供使用者查詢；宅配需多填寫住址欄；信用卡則須多填寫信用卡資料。
    (4) "完成訂購" : 這裡會再顯示一次所有購買商品的詳細資料、配送與付款方式，以及應付價格，讓使用者可以做最後的確認，按"完成"來送出訂單。

7. "Desire" : 在clothes、shoes、neckless、pants四大頁面中，每個商品右下角都有一個愛心按鈕，按下後愛心會被塗滿成紅色，代表加入慾望清單。
使用者可以透過"Desire"看到自己加到慾望清單中的所有商品，以及該商品的名稱、圖片、價格，也可以點擊商品圖片，連結到該商品的分頁，很方便！

8. 為了讓使用者的操作可以更方便，我讓使用者在任何頁面Login後，都可以直接回到原頁面，而不是回到首頁，
這樣才不會造成好不容易找到的商品，Login後又要重找一次的不方便。

9. Notification : 新的會員進入Bonny Online-store時，都會詢問是否允許通知。若允許通知，則下次再進入本網站時右下角就會跳出提示。


圖片來源 : Air space 購物網站

## Security Report (Optional)
我的database rules分成4個部分：
除了使用者外，我將其他三個部份的讀、寫的權限都設為必須登入(auth!=null)且只能寫入自己的路徑下($uid==auth.uid)，
用這些規則來確保網站的安全性。

1. 使用者:
    "users":{
        "$uid":{
            ".read": "$uid==auth.uid",
      	    ".write": "$uid==auth.uid",
        }
    }
2. 購物車:
    "items":{
    	"$uid":{
            ".read": "auth!=null && $uid==auth.uid", //for加入購物車
      	    ".write": "auth!=null && $uid==auth.uid", //for顯示購物車商品
        }
  	}
3. 訂單資料:
    "orders":{
		"$uid":{
            ".read": "auth!=null && $uid==auth.uid", //for在下單的最後一個步驟時，顯示訂單資料供使用者確認
      	    ".write": "auth!=null && $uid==auth.uid", //for將收件人資料記錄起來
        }
  	}
4. 慾望清單:
    "likes":{
		"$uid":{
            ".read": "auth!=null && $uid==auth.uid", //for記錄使用者喜歡哪個商品
      	    ".write": "auth!=null && $uid==auth.uid", //for將使用者喜歡的商品顯示在慾望清單中
        }
  	}

